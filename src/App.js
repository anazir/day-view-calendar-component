import {useState} from 'react';
import Calendar from './Calendar.tsx'
import './App.css';

function App() {
  const [offset, setOffset] = useState(8);
  const [rowHeight, setRowHeight] = useState('3rem');
  const [gutterWidth, setGutterWidth] = useState('3rem');
  const [separation, setSeparation] = useState('5px');
  const [borderLeft, setBorderLeft] = useState('5px');
  const [events, setEvents] = useState([
    {
      "id": "1",
      "title": "Boba",
      "start": 1310,
      "end": 1330
    },
    {
      "id": "",
      "title": "Call with Bob",
      "start": 420,
      "end": 480
    },
    {
      "id": "",
      "title": "Call with Bob",
      "start": 430,
      "end": 490
    },
    {
      "id": "",
      "title": "Call with Bob",
      "start": 440,
      "end": 500
    },
    {
      "id": "",
      "title": "Call with Bob",
      "start": 490,
      "end": 550
    },
    {
      "id": "2",
      "title": "Lunch",
      "start": 720,
      "end": 780
    },
    {
      "id": "3",
      "title": "Meeting with Claire",
      "start": 780,
      "end": 840
    },
    {
      "id": "4",
      "title": "Review OKRs",
      "start": 870,
      "end": 900
    },
    {
      "id": "5",
      "title": "Interview Ahmed",
      "start": 870,
      "end": 930
    },
    {
      "id": "1",
      "title": "Bob",
      "start": 1260,
      "end": 1320
    },
    {
      "id": "1",
      "title": "Bob",
      "start": 1320,
      "end": 1340
    },
    {
      "id": "1",
      "title": "Bob",
      "start": 1250,
      "end": 1270
    },
    {
      "id": "1",
      "title": "Bob",
      "start": 1270,
      "end": 1290
    },
    {
      "id": "1",
      "title": "Bob",
      "start": 1290,
      "end": 1310
    },
    {
      "id": "1",
      "title": "Call with Bob",
      "start": 1350,
      "end": 1410
    },
    {
      "id": "1",
      "title": "Call with Bob",
      "start": 1365,
      "end": 1425
    },
    {
      "id": "1",
      "title": "Call with Bob",
      "start": 1380,
      "end": 1440
    },
    {
      "id": "1",
      "title": "Call with Bob",
      "start": 1260,
      "end": 1320
    }
  ]);
  const [eText, setEText] = useState(JSON.stringify(events));

  return (
    <div className="App">
      <header className="App-header">
        <div style={{margin: '3rem', padding: '1rem'}}>
          <h1>Calendar Component</h1>

          Offset: <br/><input value={offset} onChange={e => setOffset(e.target.value)} /><br />
          Row height: <br/><input value={rowHeight} onChange={e => setRowHeight(e.target.value)} /><br />
          Gutter Width: <br/><input value={gutterWidth} onChange={e => setGutterWidth(e.target.value)} /><br />
          Separation: <br/><input value={separation} onChange={e => setSeparation(e.target.value)} /><br />
          Border Left: <br/><input value={borderLeft} onChange={e => setBorderLeft(e.target.value)} /><br />
          Events: <br/><textarea style={{width: 800, height: 400}} value={eText} onChange={e => setEText(e.target.value)} /><br />
          <button onClick={() => setEvents(JSON.parse(eText))}>Update events</button>
          <Calendar
            offset={parseInt(offset)}
            rowHeight={rowHeight}
            gutterWidth={gutterWidth}
            borderLeft={borderLeft}
            events={events}
          />
        </div>
      </header>
    </div>
  );
}

export default App;
