import styles from './Calendar.module.css';
import CSS from 'csstype';

interface Event {
  id: number;
  title: string;
  start: number;
  end: number;
  position?: number;
  index?: number;
}

interface CalendarProps {
  events?: Event[];
  style?: CSS.Properties;
  offset?: number;
  rowHeight?: string;
  gutterWidth?: string;
  separation?: string;
  borderLeft?: string;
}

// Takes an array of events returns an array of arrays of neighbours.
// Neighbours are the most events that are in collision with the current event
// including the current event.
// +-----+
// |  A  |+-----+
// |     ||  B  |+-----+
// +-----++-----+|     |
//        +-----+|  C  |
//        |  D  ||     |
//        +-----++-----+
// Here if the function call was getNeighbours([A, B, C, D]);
// the return would be:
// [ [A, B, C], [B, A, C], [C, A, B], [D, C] ]
// As you can see, C's neighbours is not just D and C but A, B and C because
// those are 3 events in collision wich is greater than D and C which is 2
const getNeighbours = (arr: Event[]): Event[][] => {
  if (arr.length < 1) {
    return [arr];
  }

  return arr.map((a: Event) => {
    let curCol: Event[] = arr.reduce((pr: Event[], cur: Event): Event[] => {
      if (cur === a) {
        return pr;
      }

      if (a.start <= cur.start && a.end > cur.start || cur.start <= a.start && cur.end > a.start) {
        return [...pr, cur];
      } else {
        return pr;
      }
    }, [] as Event[]);

    const smallest: Event[] = getNeighbours(curCol).sort((a: Event[], b: Event[]) => -a.length + b.length)[0];

    if (undefined === smallest) {
      return [a];
    }

    return [a, ...smallest];
  })
};

// Props: {
//  events: an array of events,
//  style: css styling,
//  offset: event offset from top,
//  rowHeight: height of rows,
//  gutterWidth: width of left column (time column)
//  separation: space between left and right column (time and event columns)
//  borderLeft: width of left border of events
// }
export default function Calendar({ events = [], style = {}, ...props }: CalendarProps) {
  const {
    offset = 8, // When changing row height change this as well
    rowHeight = '3rem',
    gutterWidth = '3rem',
    separation = '5px',
    borderLeft = '5px',
  } = props;

  // Convert minutes to percentage, used for css for example top: `${convert(event.start)}%`
  const convert = (val: number): number => {
    return 100 * (offset + val) / (24 * 60);
  }

  let myEvents: Event[] = [ ...events ].map((a, i) => ({ ...a, index: i}));
  const neighbours: Event[][] = getNeighbours(myEvents);

  // Assign horizontal positions to neighbouring events
  myEvents.forEach((a: Event, i) => {
    const curNeighbours = neighbours[i];
    let positions = (new Array(curNeighbours.length).fill(0).map((_, j) => j));

    if (a.position !== undefined) {
      positions = [ ...positions.slice(0, a.position), ...positions.slice(a.position + 1, positions.length)]
    }

    curNeighbours.forEach((b: Event) => {
      if (b.position !== undefined) {
        positions = [ ...positions.slice(0, b.position), ...positions.slice(b.position + 1, positions.length)]
      }
    })

    let count = 0;

    curNeighbours.forEach((b: Event) => {
      if (b.position === undefined) {
        b.position = positions[count];
        count ++;
      }
    })
  });
  ;

  const outerStyle = {
    ...style,
  };

  return (
    <div className={styles.calendar} style={outerStyle}>
      <div className={styles.wrapper}>
        {
          (new Array(24).fill(0)).map((_, i) => (
            <div className={styles.row} style={{marginTop: rowHeight}}>
              <div className={styles.gutter} style={{width: gutterWidth}}>{i >= 10 ? '' : '0'}{i}:00</div>
              <div className={styles.eventcolumn} style={{marginLeft: separation}}><hr className={styles.hr} style={{}}/></div>
            </div>
          ))
        }

        {
          myEvents.map((a: Event, i) => {
            let myIndex = a.position;
            let curNeighbours = neighbours[i].length;

            // Get the largest number of neighbours a neighbour has and assign that as curNeighbours
            neighbours[i].forEach((b: Event) => {
              if (neighbours[b.index].length > curNeighbours) {
                curNeighbours = neighbours[b.index].length;
              }
            })

            return (
              <div className={styles.event} style={{
                left: `calc(${separation} + ${gutterWidth} + ${myIndex} * ((100% - ${separation} - ${gutterWidth}) / ${curNeighbours}))`,
                borderLeft: `${borderLeft} solid #1aadf8`,
                top: `${convert(a.start)}%`,
                height: `${convert(a.end - a.start) - 100 * (offset * 1.2) / (24 * 60)}%`,
                width: `calc(${100 / (curNeighbours)}% - (${separation} + ${gutterWidth}) / ${curNeighbours} - ${borderLeft})`,
              }}>{Math.floor(a.start / 60)}:{a.start % 60 < 10 ? '0' : ''}{a.start % 60} <b>{a.title}</b></div>
            );
          })
        }

        <div style={{height: rowHeight}}></div>
      </div>
    </div>
  );
}
